Summary
-------

Simple program that just wait on TERM, QUIT and KILL signals to terminate. This
program is used for docker containers that need to wait indefinitely. Docker
send a TERM signal when the *stop* command is issued for a container. This program
will also trap the signal and terminate properly.
