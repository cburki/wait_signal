## 
## Filename     : Makefile
## Description  : 
## Author       : Christophe Burki
## Version      : 1.0.0
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License version 3 as
## published by the Free Software Foundation.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file LICENSE.  If not, write to the
## Free Software Foundation, Inc., 51 Franklin Street, Fifth
## ;; Floor, Boston, MA 02110-1301, USA.
## 
######################################################################

GOLIBS := /opt/$(shell whoami)/go_libs
GOPATH := $(GOLIBS):$(shell pwd)
TARGET := waitsig
TEST_SOURCES := $(wildcard *_test.go)
SOURCES := $(filter-out $(TEST_SOURCES), $(wildcard *.go))

#
# --------------------------------------------------------------------
#

.PHONY : all clean distclean

all : $(TARGET)


$(TARGET) : $(SOURCES)
	GOPATH=$(GOPATH) go build -o $(TARGET)

install : $(TARGET)
	cp $(TARGET) docker/registrator/.

test :
	GOPATH=$(GOPATH) go test

clean :
	rm -Rf *.o $(TARGET)

distclean : clean
	rm -f *~

vet :
	go vet $(SOURCES)
#	go vet $(TEST_SOURCES)

fmt :
	gofmt -d=true -w=true $(SOURCES)
#	gofmt -d=true -w=true $(TEST_SOURCES)
