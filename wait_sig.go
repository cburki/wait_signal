//
// Filename     : wait_sig.go
// Description  : Wait for a signal to terminate.
// Author       : Christophe Burki
// Version      : 1.0.0
//

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; see the file LICENSE.  If not, write to the
// Free Software Foundation, Inc., 51 Franklin Street, Fifth
// ;; Floor, Boston, MA 02110-1301, USA.
//

// -----------------------------------------------------------------------------

package main

// -----------------------------------------------------------------------------

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
)

// -----------------------------------------------------------------------------

func main() {

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt, syscall.SIGTERM, syscall.SIGQUIT, syscall.SIGKILL)
	for signal := range sigChan {
		fmt.Printf("Terminated with signal : %s\n", signal)
		os.Exit(0)
	}
	os.Exit(0)
}

// -----------------------------------------------------------------------------
